from unittest import main, TestCase
from io import StringIO

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
    #---------------------------------------------------------------------------
    #Solve
    #---------------------------------------------------------------------------
    def test_solve_1(self):
        r = StringIO("A Dallas hold\nB Austin move Dallas\nC NewYork support B\nD London move LosAngeles\nE LosAngeles support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Dallas\nC NewYork\nD LosAngeles\nE LosAngeles\n")

    def test_solve_2(self):
        r = StringIO("A Dallas move Rome\nB Austin move Dallas\nC NewYork move Rome\nD London move LosAngeles\nE LosAngeles support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Dallas\nC [dead]\nD LosAngeles\nE LosAngeles\n")
    
    def test_solve_3(self):
        r = StringIO("A Dallas hold\nB Austin move Dallas\nC NewYork move Dallas\nD London move Dallas\nE LosAngeles move Dallas\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\n")

#-------------------------------------------------------------------------------
#main
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    main()
